package cz.sysrq.gallerydownloader;

import org.apache.commons.io.FilenameUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Unit tests
 */
public class GalleryDownloaderTest {

    private static final Logger logger = LoggerFactory.getLogger(GalleryDownloaderTest.class);

    @Test
    public void testFile() throws MalformedURLException {
        String photoLink = "https://img.signaly.cz/photos/076000/76428/876tolx8ciounf4q3s47afisjl8s7adt.jpg";
        URL url = new URL(photoLink);
        logger.debug("Reading photo \"{}\"", url);

        String fileName = FilenameUtils.getName(url.getPath());
        Path path = Paths.get("output").resolve(fileName);
        File outputfile = new File(path.toString());
        logger.debug("Photo file saved as \"{}\"", outputfile.getAbsolutePath());

        //ImageIO.write(bufferedImage, FilenameUtils.getExtension(url.getPath()), outputfile);
        Assert.assertEquals("876tolx8ciounf4q3s47afisjl8s7adt.jpg", FilenameUtils.getName(outputfile.getPath()));
    }
}
