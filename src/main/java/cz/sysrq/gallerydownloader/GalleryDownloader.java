package cz.sysrq.gallerydownloader;

import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * Gallery Downloader main entry point.
 */
public class GalleryDownloader {

    private static final Logger logger = LoggerFactory.getLogger(GalleryDownloader.class);

    public static void main(String[] args) {
        logger.info("GalleryDownloader {} (params count {})", args, args.length);

        // TODO: make this it better
        String cmd = null;
        String url = null;
        String boxClass = null;

        if (args.length < 2) {
            System.err.println("Usage: GalleryDownloader <cmd> <link>");
            System.err.println("  cmd:   l = lookup for galleries");
            System.err.println("  cmd:   d = download gallery");
        } else if (args.length == 2) {
            cmd = args[0].toUpperCase();
            url = args[1];
        } else if (args.length == 3) {
            cmd = args[0].toUpperCase();
            url = args[1];
            boxClass = args[2];
        }

        logger.info("CMD={} LINK={} CLASS={}", cmd, url, boxClass);

        if ("L".equals(cmd)) {
            WebDriver driver = new HtmlUnitDriver();
            findGalleriesUrl(url, driver);
            driver.quit();
        } else if ("D".equals(cmd)) {
            WebDriver driver = new HtmlUnitDriver();
            downloadGallery(driver, url, boxClass, Paths.get("photos"));
            driver.quit();
        } else {
            System.err.println("Unknown command: " + cmd);
        }

    }

    /**
     * Finds galleries on the web - this should be customized per website.
     *
     * @param urlStr website URL
     * @param driver WebDriver
     * @return list of galleries URL
     */
    private static List<String> findGalleriesUrl(String urlStr, WebDriver driver) {
        List<String> galleriesUrl = new ArrayList<>();

        driver.get(urlStr);
        logger.debug("Page title \"{}\"", driver.getTitle());
        logger.debug("> H1: \"{}\"", driver.findElement(By.tagName("h1")).getText());

        // find the Class Pages links
        WebElement table = driver.findElement(By.tagName("table"));
        List<String> linksUrl = findLinks(table);
        logger.debug("Found {} links: {}", linksUrl.size(), linksUrl);
        for (String href : linksUrl) {
            driver.get(href);
            logger.debug("Page title \"{}\"", driver.getTitle());
            logger.debug("> H1: \"{}\"", driver.findElement(By.tagName("h1")).getText());
            List<WebElement> tables = driver.findElements(By.tagName("table"));
            // first table should be Class Summary
            WebElement table1 = tables.get(0);
            logger.debug("> tab1: {}", table1.findElement(By.tagName("caption")).getText());
            List<WebElement> tab1Rows = table1.findElements(By.tagName("tr"));
            for (WebElement tr : tab1Rows) {
                logger.debug(">> tr {}", tr.getText());
            }
            // second table should be the photo galleries
            WebElement table2 = tables.get(1);
            logger.debug("> tab2: {}", table2.findElements(By.tagName("th")).get(0).getText());
            List<WebElement> tab2Rows = table2.findElements(By.tagName("tr"));
            for (WebElement tr : tab2Rows) {
                logger.debug(">> tr {}", tr.getText());
            }
            // find links in the 2nd table
            List<String> galleryLinks = findLinks(table2);
            galleriesUrl.addAll(galleryLinks);
        }
        return galleriesUrl;
    }

    private static void downloadGallery(WebDriver driver, String galleryLink, String boxClass, Path outputPath) {
        driver.get(galleryLink);
        WebElement photos = driver.findElement(By.className(boxClass));
        List<String> photoLinks = findLinks(photos);
        for (String photoLink : photoLinks) {
            try {
                logger.debug("Photo link \"{}\"", photoLink);
                URL url = new URL(photoLink);
                logger.debug("Reading photo \"{}\"", url);
                String fileName = FilenameUtils.getName(url.getPath());
                Path path = outputPath.resolve(fileName);
                logger.debug("Photo file will be saved to \"{}\"", path);
                File outputfile = new File(path.toString());
                BufferedImage bufferedImage = ImageIO.read(url);
                ImageIO.write(bufferedImage, FilenameUtils.getExtension(url.getPath()), outputfile);
                logger.debug("Photo file saved as \"{}\"", outputfile.getAbsolutePath());
            } catch (IOException e) {
                logger.error("Cannot download image: " + e.getLocalizedMessage(), e);
            }
        }
    }

    private static List<String> findLinks(WebElement e) {
        List<WebElement> links = e.findElements(By.tagName("a"));
        List<String> urls = new ArrayList<>(links.size());
        for (WebElement link : links) {
            String href = link.getAttribute("href");
            logger.debug("Link \"{}\" url \"{}\"", link.getText(), href);
            urls.add(href);
        }
        return urls;
    }
}
