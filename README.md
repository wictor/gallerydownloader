Gallery Downloader
==================

Simple console tool extracting images from a web gallery. Should be customized for a particular web.
Using Selenium WebDriver.

License
-------
Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).